

def givewidths(): #weergeeft de breedtes van de PCB's
   import mysql.connector as mariadb
   import re
   mariadb_connection = mariadb.connect(user='benoit',
                                        password='newtec',
                                        database='pcb_test')
   cursor = mariadb_connection.cursor()
   width = []
   cursor.execute("SELECT breedte FROM pcbs;")
   unwanted = "[),(]"
   for breedte in cursor:
      wanted = re.sub(unwanted, '', str(breedte))
      width.append(wanted)
   return width


def giveids(): #weergeeft de id's van de PCB's
   import mysql.connector as mariadb
   import re
   mariadb_connection = mariadb.connect(user='benoit',
                                        password='newtec',
                                        database='pcb_test')
   cursor = mariadb_connection.cursor()
   width1 = []
   cursor.execute("SELECT ID FROM pcbs;")
   unwanted = "[),(]"
   for breedte in cursor:
      wanted = re.sub(unwanted, '', str(breedte))
      width1.append(wanted)
   return width1


def givecompletetable(): #weergeeft de complete table
   import mysql.connector as mariadb
   import re
   mariadb_connection = mariadb.connect(user='benoit',
                                        password='newtec',
                                        database='pcb_test')
   cursor = mariadb_connection.cursor()
   tables = []
   cursor.execute("SELECT * FROM pcbs;")
   for value in cursor:
      tables.append(value)
   return tables


def insertvalues(ID, breedte, beschrijving): #wordt gebruikt om waarden in de database te plaatsten
   import mysql.connector as mariadb
   import re
   mariadb_connection = mariadb.connect(user='benoit',
                                        password='newtec',
                                        database='pcb_test')
   cursor = mariadb_connection.cursor()
   query = """INSERT INTO pcbs (ID, breedte, beschrijving) VALUES (%s, %s, %s) """
   recordTuple = (ID, breedte, beschrijving)
   cursor.execute(query, recordTuple)
   mariadb_connection.commit()
   return













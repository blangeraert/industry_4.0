from tkinter import *
from tkinter import messagebox
import tkinter as tk
from time import sleep
import time
import tkinter.font as tkFont
#import i2c_lib as i2c
#import database_lib as database
#import Rs_485_database_lib as rs485
import os


getal = 0 ## start width, width of 'tussenplaat' and start position ##
value = 100 ## wanted width in custom PCB ##
running = False
jobid = None
jobid2 = None


def start_motor(direction):
    print("starting motor...(%s)" % direction)
    addmove(direction)


def sendprecision(value):
    rs485.send_precision(value)
    time.sleep(0.1)
    i2c.pulseinputrelais1()
    time.sleep(0.1)
    return


def addmove(direction):
    global jobid
    global value
    print("Moving (%s)" % direction)
    jobid = master.after(50, addmove, direction)
    global getal
    getal += value
    print(getal)
    sendprecision(getal)
    print(getal)
    print("+ distance")


def start_motor2(direction2):
    print("starting motor...(%s)" % direction2)
    addmove2(direction2)


def addmove2(direction2):
    global jobid2
    global value
    global getal
    print("Moving (%s)" % direction2)
    jobid2 = master.after(50, addmove2, direction2)
    getal -= value
    print(getal)
    sendprecision(getal)
    print("+ distance2")


def home():
    global getal
    i2c.pulseinputrelais2()
    getal = 0


def stopmotion():
    global jobid
    master.after_cancel(jobid)


def stopmotion2():
    global jobid2
    master.after_cancel(jobid2)


def defstop():
    i2c.pulseinputrelais3()
    print("stop")
    time.sleep(.1)


def addwidthtodatabase():
    global getal
    array = database.giveids()
    for i in range(0, len(array)):
        array[i] = int(array[i])
    newid = max(array) + 1 ## newest ID
    print(newid)
    print("old: ", getal)
    print("new: ", round((getal/667.111)+50.7), 2)
    newgetal = round(((getal/667.111)+50.7),2)
    descr = f'PCB_TEST_{newid}'
    print(descr)
    database.insertvalues(newid, newgetal, descr) ##AD NEW VALUE TO DATABASE!! :D ##


def costum_pcb():
    global getal
    global value
    windowc = tk.Toplevel(master)
    windowc.geometry("490x570")

    x = IntVar()
    x.set(value)
    print(getal)

    for direction in ("forward"):
        plusbutton = tk.Button(windowc, text="+", bg="yellow", font=fontStyleButtonExtra)
        plusbutton.grid(row=0, column=0, sticky="we", pady=2, padx=2)
        plusbutton.bind('<ButtonPress-1>', lambda event, direction=direction: start_motor(direction))
        plusbutton.bind('<ButtonRelease-1>', lambda event: stopmotion())

    for direction2 in ("backward"):
        minusbutton = tk.Button(windowc, text="-", bg="yellow", font=fontStyleButtonExtra)
        minusbutton.grid(row=1, column=0, sticky="we", pady=2, padx=2)
        minusbutton.bind('<ButtonPress-1>', lambda event, direction2=direction2: start_motor2(direction2))
        minusbutton.bind('<ButtonRelease-1>', lambda event: stopmotion2())


    def messagebox():
        MsgBox2 = tk.messagebox.askquestion('add new value?', 'Are u sure?', icon='warning', parent=windowc)

        if MsgBox2 == 'yes':
            try:
                addwidthtodatabase()
            except:
                tk.messagebox.showinfo('Return', 'ERROR', parent=windowc, icon='warning')
                print("Something went wrong ...")
        else:
            tk.messagebox.showinfo('Return', 'You will now return to the application screen', parent=windowc)
        return

    def lessvalue():
        global value
        value = value / 2
        x.set(value)

    def morevalue():
        global value
        value = value * 2
        x.set(value)



    plusvaluebutton = tk.Button(windowc, text="Value +", bg="grey", command=(lambda: morevalue()), font=fontStyleButtonExtra)
    plusvaluebutton.grid(row=2, column=0, sticky="we", pady=2)

    label1 = Label(windowc, textvariable=x, font=("Lucida Grande", 20))
    label1.grid(row=3, column=0, sticky="we", pady=2)


    minvaluebutton = tk.Button(windowc, text="Value -", bg="grey", command=(lambda: lessvalue()), font=fontStyleButtonExtra)
    minvaluebutton.grid(row=4, column=0, sticky="we", pady=2)




    Addbutton = tk.Button(windowc, text="Add value", bg="orange", command=(lambda: messagebox()), font=fontStyleButton)
    Addbutton.grid(row=5, column=0, sticky="we", pady=2)

    Exitbutton1 = tk.Button(windowc, text="Exit", bg="grey", command=windowc.destroy, font=fontStyleButtonExtra)
    Exitbutton1.grid(row=6, column=0, sticky="we", pady=2)
    return


def shutdown():
    MsgBox1 = tk.messagebox.askquestion('New query', 'Are u sure?', icon='warning', parent=master)

    if MsgBox1 == 'yes':
        try:
            os.system("sudo shutdown -h now")
        except:
            tk.messagebox.showinfo('Return', 'ERROR', parent=master, icon='warning')
            print("Something went wrong ...")
    else:
        tk.messagebox.showinfo('Return', 'You will now return to the application screen', parent=master)
    return


def create_window():
    window = tk.Toplevel(master)
    window.geometry("490x570")
    listbox = Listbox(window, font=fontStyleList)

    yscroll = tk.Scrollbar(window, command=listbox.yview, orient=tk.VERTICAL)
    yscroll.grid(row=0, column=1, sticky='nswe', padx=20)
    listbox.configure(yscrollcommand=yscroll.set)

    a = 0

    def getcurrentval():
        global getal
        # window.withdraw()
        MsgBox = tk.messagebox.askquestion('New query', 'Are u sure?', icon='warning', parent=window)

        if MsgBox == 'yes':
            value = listbox.get(listbox.curselection())
            str = value.split(',')  # GETS CURRENT WIDTH VALUE OF PCB
            try:
                rs485.send_normal(float(str[1]))
                #getal = float(str[1])
                getal = rs485.getgetal()
                tk.messagebox.showinfo('Return', 'Data was send succesfully', parent=window)
            except:
                tk.messagebox.showinfo('Return', 'ERROR', parent=window, icon='warning')
                print("Something went wrong ...")
        else:
            tk.messagebox.showinfo('Return', 'You will now return to the application screen', parent=window)

    for i in database.givewidths():
        listbox.insert(a, str(database.givecompletetable()[a]))
        a = a + 1

    listbox.grid(row=0, column=0, sticky="ew")
    Selectbutton = tk.Button(window, text="Select", bg="white", command=(lambda: getcurrentval()), font=fontStyleButtonExtra)
    Selectbutton.grid(row=1, column=0, sticky="ew", pady=2)
    Exitbutton1 = tk.Button(window, text="Exit", bg="white", command=window.destroy, font=fontStyleButtonExtra)
    Exitbutton1.grid(row=2, column=0, sticky="ew", pady=2)


master = tk.Tk()
master.title("NewTec follow-me software")
master.geometry("490x570")


fontStyleButton = tkFont.Font(family="Lucida Grande", size=38)
fontStyleButtonExtra = tkFont.Font(family="Lucida Grande", size=45)
fontStyleList = tkFont.Font(family="Lucida Grande", size=21)

ONbutton = tk.Button(master, text="Start", bg="lime", command=(lambda: i2c.pulseinputrelais1()), font=fontStyleButton)
ONbutton.grid(row=0, column=0, sticky="ew", pady=2)

OFFbutton = tk.Button(master, text="Home", bg="blue", command=(lambda: home()), font=fontStyleButton)
OFFbutton.grid(row=1, column=0, sticky="ew", pady=2)

STOPbutton = tk.Button(master, text="Stop", bg="red", command=(lambda: defstop()), font=fontStyleButton)
STOPbutton.grid(row=2, column=0, sticky="ew", pady=2)

CreateNewWindows = tk.Button(master, text="Display database", bg="orange", command=(lambda: create_window()),
                             font=fontStyleButton)
CreateNewWindows.grid(row=3, column=0, sticky="ew", pady=2)

Costumbutton = tk.Button(master, text="Custom PCB", bg="purple", command=(lambda: costum_pcb()), font=fontStyleButton)
Costumbutton.grid(row=4, column=0, sticky="ew", pady=2)

# Exitbutton = tk.Button(master, text="Exit",bg="grey", command=master.destroy, font=fontStyleButton)
# Exitbutton.grid(row=4, column=0, sticky="ew", pady=2)

shutdownbutton = tk.Button(master, text="Shutdown system", bg="grey", command=(lambda: shutdown()),
                           font=fontStyleButton)
shutdownbutton.grid(row=5, column=0, sticky="ew", pady=70)

master.mainloop()

import libscrc
import serial
import database_lib as pcb_width
#import i2c_lib as i2c

#i2c.testallinputrelais() test all relais (total of 3) do not use when driver is attachted!

# print("-----------------------")
# print(*pcb_width.givecompletetable(), sep='\n')
# print("-----------------------")

# insertornot = input("Wish to add data to the database? (y/n)")
#
# if insertornot == 'y':
#     id1 = input("ID please: ")
#     width1 = input("width please: ")
#     description1 = input("description please: ")
#     pcb_width.insertvalues(id1, width1, description1)
#     print("VALUE ADDED TO DATABASE! ")
# else:
#     print("No data added to database ")

# print("-----------------------")
newpos = 0


def send_precision(value):
    global newpos
    send_rs485(value)
    return


def getgetal():
    global newpos
    return newpos


def send_normal(value):
    global newpos
    print("width: ", "667.111 x (", value, " - 50.7mm) equals ", ((float(value) - 50.7) * 667.111), " steps. ")
    newpos = (float(value) - 50.7) * 667.111
    send_rs485(newpos)
    return


def send_rs485(value):
    #ComPort = serial.Serial('COM5') # open COM24
    ComPort = serial.Serial('/dev/ttyUSB0')
    ComPort.baudrate = 9600  # set Baud rate to 9600
    ComPort.bytesize = 8    # Number of data bits = 8
    ComPort.parity = 'E'  # No parity
    ComPort.stopbits = 1    # Number of Stop bits = 1


    print("Baudrate: ", ComPort.baudrate)
    print("Bytesize: ", ComPort.bytesize)
    print("Parity: ", ComPort.parity)
    print("Stopbits: ", ComPort.stopbits)

            #adr#                                      #positiooooooooooooon#  #speeeeeeeeeeeeeeeeed#
    data1 = [0x04, 0x10, 0x18, 0x02, 0x00, 0x04, 0x08, 0x00, 0x00, 0x03, 0xE8, 0x00, 0x00, 0x13, 0x88]

    ######################################################################### start position!
    #newpre = input("Which PCB do you need? (enter an ID number): ")

    #newpre2 = pcb_width.givewidths()[int(newpre) - 1]

    newpos2 = hex((int)(value))
    print("hexadecimal value: ", newpos2)
    #print(len(newpos2))

    if len(newpos2) <= 0:
        raise ValueError('error length')
    elif len(newpos2) == 3:
        newvaluepos = newpos2[0] + newpos2[1] + newpos2[2]
        data1[10] = int(newvaluepos, 16)
        data1[9] = 0
    elif len(newpos2) == 4:
        newvaluepos = newpos2[0] + newpos2[1] + newpos2[2] + newpos2[3]
        data1[10] = int(newvaluepos, 16)
        data1[9] = 0
    elif len(newpos2) == 5:
        newvaluepos1 = newpos2[0] + newpos2[1] + newpos2[3] + newpos2[4]
        newvaluepos2 = newpos2[0] + newpos2[1] + newpos2[2]
        print(newvaluepos1)
        print(newvaluepos2)
        data1[10] = int(newvaluepos1, 16)
        data1[9] = int(newvaluepos2, 16)
    elif len(newpos2) == 6:
        newvaluepos1 = newpos2[0] + newpos2[1] + newpos2[4] + newpos2[5]
        newvaluepos2 = newpos2[0] + newpos2[1] + newpos2[2] + newpos2[3]
        data1[10] = int(newvaluepos1, 16)
        data1[9] = int(newvaluepos2, 16)
    elif len(newpos2) == 7:
        newvaluepos1 = newpos2[0] + newpos2[1] + newpos2[5] + newpos2[6]
        newvaluepos2 = newpos2[0] + newpos2[1] + newpos2[3] + newpos2[4]
        newvaluepos3 = newpos2[0] + newpos2[1] + newpos2[2]
        data1[10] = int(newvaluepos1, 16)
        data1[9] = int(newvaluepos2, 16)
        data1[8] = int(newvaluepos3, 16)
    elif len(newpos2) == 8:
        newvaluepos1 = newpos2[0] + newpos2[1] + newpos2[6] + newpos2[7]
        newvaluepos2 = newpos2[0] + newpos2[1] + newpos2[4] + newpos2[5]
        newvaluepos3 = newpos2[0] + newpos2[1] + newpos2[2] + newpos2[3]
        data1[10] = int(newvaluepos1, 16)
        data1[9] = int(newvaluepos2, 16)
        data1[8] = int(newvaluepos3, 16)
    elif len(newpos2) == 9:
        newvaluepos1 = newpos2[0] + newpos2[1] + newpos2[7] + newpos2[8]
        newvaluepos2 = newpos2[0] + newpos2[1] + newpos2[5] + newpos2[6]
        newvaluepos3 = newpos2[0] + newpos2[1] + newpos2[3] + newpos2[4]
        newvaluepos4 = newpos2[0] + newpos2[1] + newpos2[2]
        data1[10] = int(newvaluepos1, 16)
        data1[9] = int(newvaluepos2, 16)
        data1[8] = int(newvaluepos3, 16)
        data1[7] = int(newvaluepos4, 16)
    elif len(newpos2) == 10:
        newvaluepos1 = newpos2[0] + newpos2[1] + newpos2[8] + newpos2[9]
        newvaluepos2 = newpos2[0] + newpos2[1] + newpos2[6] + newpos2[7]
        newvaluepos3 = newpos2[0] + newpos2[1] + newpos2[4] + newpos2[5]
        newvaluepos4 = newpos2[0] + newpos2[1] + newpos2[2] + newpos2[3]
        data1[10] = int(newvaluepos1, 16)
        data1[9] = int(newvaluepos2, 16)
        data1[8] = int(newvaluepos3, 16)
        data1[7] = int(newvaluepos4, 16)
    ########################################################################### end position!
    print("-----------------------")
    print("##value of position given##")
    print("-----------------------")
    ######################################################################### start speed!
    # speed = input("How fast? (1000Hz ~ 1sec) ")
    print("fixed speed: 5000Hz")
    speed2 = hex((int)(5000))
    print("Hexadecimal value: ", speed2)
    #print(len(speed2))

    if len(speed2) <= 0:
        raise ValueError('error speed')
    elif len(speed2) == 3:
        newvaluespd = speed2[0] + speed2[1] + speed2[2]
        data1[14] = int(newvaluespd, 16)
        data1[13] = 0
    elif len(speed2) == 4:
        newvaluespd = speed2[0] + speed2[1] + speed2[2] + speed2[3]
        data1[14] = int(newvaluespd, 16)
        data1[13] = 0
    elif len(speed2) == 5:
        newvaluespd1 = speed2[0] + speed2[1] + speed2[3] + speed2[4]
        newvaluespd2 = speed2[0] + speed2[1] + speed2[2]
        data1[14] = int(newvaluespd1, 16)
        data1[13] = int(newvaluespd2, 16)
    elif len(speed2) == 6:
        newvaluespd1 = speed2[0] + speed2[1] + speed2[4] + speed2[5]
        newvaluespd2 = speed2[0] + speed2[1] + speed2[2] + speed2[3]
        data1[14] = int(newvaluespd1, 16)
        data1[13] = int(newvaluespd2, 16)
    elif len(speed2) == 7:
        newvaluespd1 = speed2[0] + speed2[1] + speed2[5] + speed2[6]
        newvaluespd2 = speed2[0] + speed2[1] + speed2[3] + speed2[4]
        newvaluespd3 = speed2[0] + speed2[1] + speed2[2]
        data1[14] = int(newvaluespd1, 16)
        data1[13] = int(newvaluespd2, 16)
        data1[12] = int(newvaluespd3, 16)
    elif len(speed2) == 8:
        newvaluespd1 = speed2[0] + speed2[1] + speed2[6] + speed2[7]
        newvaluespd2 = speed2[0] + speed2[1] + speed2[4] + speed2[5]
        newvaluespd3 = speed2[0] + speed2[1] + speed2[2] + speed2[3]
        data1[14] = int(newvaluespd1, 16)
        data1[13] = int(newvaluespd2, 16)
        data1[12] = int(newvaluespd3, 16)
    elif len(speed2) == 9:
        newvaluespd1 = speed2[0] + speed2[1] + speed2[7] + speed2[8]
        newvaluespd2 = speed2[0] + speed2[1] + speed2[5] + speed2[6]
        newvaluespd3 = speed2[0] + speed2[1] + speed2[3] + speed2[4]
        newvaluespd4 = speed2[0] + speed2[1] + speed2[2]
        data1[14] = int(newvaluespd1, 16)
        data1[13] = int(newvaluespd2, 16)
        data1[12] = int(newvaluespd3, 16)
        data1[11] = int(newvaluespd4, 16)
    elif len(speed2) == 10:
        newvaluespd1 = speed2[0] + speed2[1] + speed2[8] + speed2[9]
        newvaluespd2 = speed2[0] + speed2[1] + speed2[6] + speed2[7]
        newvaluespd3 = speed2[0] + speed2[1] + speed2[4] + speed2[5]
        newvaluespd4 = speed2[0] + speed2[1] + speed2[2] + speed2[3]
        data1[14] = int(newvaluespd1, 16)
        data1[13] = int(newvaluespd2, 16)
        data1[12] = int(newvaluespd3, 16)
        data1[11] = int(newvaluespd4, 16)
    ######################################################################### end speed!
    print("-----------------------")
    print("##value of speed given##")
    print("-----------------------")
    ######################################## START CRC16 CHECKSUM
    crc16 = libscrc.modbus(bytes(data1))

    str = hex(crc16)
    #print(len(str))
    print("CRC 16 CHECKSUM: ", str)

    if len(str) == 3:
        str1 = str[0] + str[1] + str[2]
        str2 = int(str1, 16)
        data1.insert(15, str2)
        data1.insert(16, 0)
        print(str2)
    elif len(str) == 4:
        str1 = str[0] + str[1] + str[2] + str[3]
        str2 = int(str1, 16)
        data1.insert(15, str2)
        data1.insert(16, 0)
        print(str2)
    elif len(str) == 5:
        str1 = str[0] + str[1] + str[3] + str[4]
        str2 = str[0] + str[1] + str[2]
        str3 = int(str1, 16)
        str4 = int(str2, 16)
        data1.insert(15, str3)
        data1.insert(16, str4)
        print(str3)
        print(str4)
    elif len(str) == 6:
        str1 = str[0] + str[1] + str[2] + str[3]
        str2 = str[0] + str[1] + str[4] + str[5]
        str3 = int(str1, 16)
        str4 = int(str2, 16)
        data1.insert(16, str3)
        data1.insert(15, str4)
    ######################################## END CRC16 CHECKSUM

    print("---CRC16---")

    print("Deliverd query: ", data1)

    No = ComPort.write(data1)

    ComPort.close()
    return
from smbus2 import SMBus
import time


def pulseinputrelais1():
    with SMBus(1) as bus:
        bus.write_byte_data(0x3F, 0, 101)  # relais 1 gaat aan
        time.sleep(0.5)
        bus.write_byte_data(0x3F, 0, 111)  # relais 1 gaat uit
    return


def pulseinputrelais2():
    with SMBus(1) as bus:
        bus.write_byte_data(0x3F, 0, 102)  # relais 2 gaat aan
        time.sleep(0.5)
        bus.write_byte_data(0x3F, 0, 112)  # relais 2 gaat uit
    return


def pulseinputrelais3():
    with SMBus(1) as bus:
        bus.write_byte_data(0x3F, 0, 103)  # relais 3 gaat aan
        time.sleep(0.5)
        bus.write_byte_data(0x3F, 0, 113)  # relais 3 gaat uit
    return


def testallinputrelais():  # alle relais uit testen
    pulseinputrelais1()
    pulseinputrelais2()
    pulseinputrelais3()
    return




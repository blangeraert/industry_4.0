import RPi.GPIO as GPIO
import time
import socket


GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
LED1 = 2
LED2 = 3
knop1= 6
knop2 = 13
knop3 = 19
knop4 = 26
GPIO.setup(12, GPIO.OUT)
GPIO.setup(LED1, GPIO.OUT)
GPIO.setup(LED2, GPIO.OUT)
GPIO.setup(knop1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(knop2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(knop3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(knop4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
p = GPIO.PWM(12, 50)  # channel=12 frequency=50Hz
p.start(0)

upstream = "192.168.0.184"
downstream = "192.168.0.227"
TCP_PORT = 5005
BUFFER_SIZE1 = 1024
BUFFER_SIZE2 = 1024



while True:
    MESSAGE1 = "message to UPstream"
    MESSAGE2 = "message to DOWNstream"


    if GPIO.input(knop1) == 0:
        s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s1.connect((upstream, TCP_PORT))
        s1.send(str(MESSAGE1))
        data1 = s1.recv(BUFFER_SIZE1)
        s1.close()
        print("received data:", str(data1))
        u = 0
        GPIO.output(LED1, GPIO.HIGH)
        print("--------------------")
        print("UPSTREAM -> CONVEYOR")
        print("--------------------")
        p.ChangeDutyCycle(100)
        time.sleep(2)
        p.ChangeDutyCycle(0)
        GPIO.output(LED1, GPIO.LOW)
        while u == 0:
            if GPIO.input(knop4) == 0:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((downstream, TCP_PORT))
                s.send(str(MESSAGE2))
                data2 = s.recv(BUFFER_SIZE2)
                s.close()
                print("received data:", str(data2))
                GPIO.output(LED2, GPIO.HIGH)
                print("----------------------")
                print("CONVEYOR -> DOWNSTREAM")
                print("----------------------")
                print("######################")
                p.ChangeDutyCycle(100)
                time.sleep(2)
                p.ChangeDutyCycle(0)
                GPIO.output(LED2, GPIO.LOW)
                u = u + 1
    else:
        GPIO.output(LED1, GPIO.LOW)
        GPIO.output(LED2, GPIO.LOW)





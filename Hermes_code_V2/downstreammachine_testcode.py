from datetime import datetime
import RPi.GPIO as GPIO
import time
import socket
import select

upstream = "192.168.0.153"
TCP_PORT = 50101
BUFFER_SIZE1 = 1024

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

knop1 = 6
knop2 = 26
motor = 21

output = 23
input = 22


GPIO.setup(output, GPIO.OUT)
GPIO.setup(motor, GPIO.OUT)
GPIO.setup(input, GPIO.IN, pull_up_down=GPIO.PUD_UP)


GPIO.setup(knop1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(knop2, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def getRequest(data):
    request = data.decode("utf-8")
    request1 = request.split(' ')
    return request1


def checkAlive():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    checkAliveResponse = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">"
                          + str.encode('\n') + b" <" + b"CheckAlive Type=" + str.encode("\"") + b"2" + str.encode("\"")
                          + b" /" + b">" + str.encode('\n') + b"</Hermes>") + str.encode('\n')
    return checkAliveResponse


def serviceDescriptionDownstream():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b" <ServiceDescription LaneId=" + str.encode("\"") + b"1" + str.encode("\"") + b" MachineId=" + str.encode("\"") + b"downstream machine" + str.encode("\"") +
               b" Version=" + str.encode("\"") + b"1.2" + str.encode("\"") + b">" + str.encode('\n') +
               b"  <SupportedFeatures />" + str.encode('\n') + b" </ServiceDescription>" + str.encode('\n') + b"</Hermes>") + str.encode('\n')
    return request


def machineReady():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b"<MachineReady FailedBoard=" + str.encode("\"") + b"0" + str.encode("\"") + b" FlippedBoard=" + str.encode("\"") + b"0" + str.encode("\"") + b" />" + str.encode('\n') +
               b"</Hermes>")
    return request


def startTransport():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b"<StartTransport BoardId=" + str.encode("\"") + b"555" + str.encode("\"") + b" />" + str.encode('\n') +
               b"</Hermes>")
    return request


def stopTransport():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b"<StopTransport TransferState=" + str.encode("\"") + b"3" + str.encode("\"") + b" BoardId=" +
               str.encode("\"") + b"555" + str.encode("\"") + b" />" + str.encode('\n') +
               b"</Hermes>")
    return request


try:
    s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s1.connect((upstream, TCP_PORT))
    print("connected to upstream machine...")
    while True:
        GPIO.output(output, GPIO.LOW)
        data1 = s1.recv(BUFFER_SIZE1)
        response0 = getRequest(data1)
        #print(response0)
        if response0[2] == "<CheckAlive":
             print("checkAlive message received")
             s1.send(checkAlive())
             print("checkalive message was send to upstream")
             time.sleep(2)
             s1.send(serviceDescriptionDownstream())  # send servicedescription to upstream
             print("servicediscription to upstream was send ...")
             data2 = s1.recv(BUFFER_SIZE1) # wait upstream service description
             response = getRequest(data2)
             if response[4] == "MachineId=\"Upstream":
                print("downstream to upstream connection established ...")
                while True: # while it this loop we have a constantly connected stream, which enables us to handover PCB's with this loop only!
                    if GPIO.input(knop1) == 0:
                        s1.send(machineReady())
                        print("machineReady was send  ...")
                        data3 = s1.recv(BUFFER_SIZE1)  # wait for boardAvailable
                        response2 = getRequest(data3)
                        if response2[2] == "<BoardAvailable":
                            print("board is available from upstream, ready for start transport ...")
                            a = 0
                            while a == 0:
                                if GPIO.input(knop2) == 0: # this is te transport button, goes when pushed, stops when released
                                    s1.send(startTransport()) # sends a start transport signal
                                    while GPIO.input(knop2) == 0: # stops when button is released
                                        GPIO.output(motor, GPIO.HIGH)
                                        print("Transport started")
                                        time.sleep(0.2)
                                    a = a + 1
                                    GPIO.output(motor, GPIO.LOW)
                            s1.send(stopTransport())
                            print("transport has stopped ...")
                            data4 = s1.recv(BUFFER_SIZE1)  # wait for transportFinished repsonse
                            respone3 = getRequest(data4)
                            if respone3[2] == "<TransportFinished":
                                print("transport finished") # now that the PCB is on the conveyor, its ready to be send to the downstream machine.
                                h = 0
                                while h == 0:
                                    GPIO.output(output, GPIO.HIGH)
                                    print("PCB available on conveyor and waiting until PCB is handed over to the downstream machine")
                                    if GPIO.input(input) == 0:
                                        h = h + 1
                                        print("PCB has left conveyor towards downstream")
                                        GPIO.output(output, GPIO.LOW)
                                    time.sleep(1)
except socket.error as exc:
    print("Caught exception socket.error : %s" % exc)

except KeyboardInterrupt:
    print("\n" + "session ended by user")
    GPIO.output(output, GPIO.LOW)
    s1.close()
    print("connection closed")

finally:
    GPIO.cleanup()

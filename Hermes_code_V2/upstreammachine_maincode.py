from datetime import datetime
import RPi.GPIO as GPIO
import time
import socket
import select

TCP_PORT = 50101
BUFFER_SIZE1 = 1024

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

boardAvailableSMEMA = 13
machineReadySMEMA = 26

led_up_groen = 16
led_up_rood = 21

input = 24
output = 27

GPIO.setup(boardAvailableSMEMA, GPIO.OUT)
GPIO.setup(machineReadySMEMA, GPIO.IN)

GPIO.setup(led_up_groen, GPIO.OUT)
GPIO.setup(led_up_rood, GPIO.OUT)

GPIO.setup(input, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(output, GPIO.OUT)




def getRequest(data):
    request = data.decode("utf-8")
    request1 = request.split(' ')
    return request1


def checkAlive():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    checkAliveResponse = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">"
                          + str.encode('\n') + b" <" + b"CheckAlive Type=" + str.encode("\"") + b"2" + str.encode("\"")
                          + b" /" + b">" + str.encode('\n') + b"</Hermes>") + str.encode('\n')
    return checkAliveResponse


def serviceDescriptionUpstream():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b" <ServiceDescription LaneId=" + str.encode("\"") + b"1" + str.encode("\"") + b" MachineId=" + str.encode("\"") + b"Upstream machine" + str.encode("\"") +
               b" Version=" + str.encode("\"") + b"1.2" + str.encode("\"") + b">" + str.encode('\n') +
               b"  <SupportedFeatures />" + str.encode('\n') + b" </ServiceDescription>" + str.encode('\n') + b"</Hermes>") + str.encode('\n')
    return request


def boardAvailable():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b"<BoardAvailable BoardId=" + str.encode("\"") + b"555" + str.encode("\"") + b" BoardIdCreatedBy=" + str.encode("\"") + b"Upstream machine" + str.encode("\"") + b" FailedBoard=" + str.encode("\"") + b"0" + str.encode("\"") + b" FlippedBoard=" + str.encode("\"") + b"0" + str.encode("\"") + b" />" + str.encode('\n') +
               b"</Hermes>") + str.encode('\n')
    return request


def transportFinished():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b"<TransportFinished TransferState=" + str.encode("\"") + b"3" + str.encode("\"") + b" BoardId=" +
               str.encode("\"") + b"555" + str.encode("\"") + b" />" + str.encode('\n') +
               b"</Hermes>")
    return request


try:
    GPIO.output(output, GPIO.LOW)
    GPIO.output(boardAvailableSMEMA, GPIO.LOW) # sets boardavailable high
    GPIO.output(led_up_groen, GPIO.HIGH)
    GPIO.output(led_up_rood, GPIO.LOW)
    s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s1.bind(('', TCP_PORT))
    print("connected to %s" %(TCP_PORT))
    s1.listen(5)
    print("socket is listening")
    c, addr = s1.accept()
    print("got connection from ", addr)
    data1 = c.recv(BUFFER_SIZE1) # waiting for servicedescription from downstream
    data2 = getRequest(data1)
    if data2[2] == "<ServiceDescription":
        print("Upstream got service description message from downstream")
        time.sleep(5)
        c.send(serviceDescriptionUpstream())
        print("service description was send from upstream to downstream")
        print("upstream to downstream connection established!")
        while True:
            print("waiting for machine ready message")
            data3 = c.recv(BUFFER_SIZE1)
            data4 = getRequest(data3)
            if data4[2] == "<MachineReady":
                print("machineready message from downstream")
                if GPIO.input(machineReadySMEMA) == 0: # waiting for the conveyor to be machineready
                    print("downstream is machineready")
                    c.send(boardAvailable())
                    print("sending boardAvailable message to upstream")
                    if GPIO.input(input) == 0: # there needs to be a PCB available on the conveyor
                        data5 = c.recv(BUFFER_SIZE1)
                        data6 = getRequest(data5)
                        if data6[2] == "<StartTransport":
                            GPIO.output(led_up_groen, GPIO.LOW)
                            GPIO.output(led_up_rood, GPIO.HIGH)
                            print("transport has started ...")
                            GPIO.output(boardAvailableSMEMA, GPIO.HIGH)  # transport started!
                            print("transport started")
                            delay(1)
                            GPIO.output(boardAvailableSMEMA, GPIO.LOW)  # transport started!
                            g = 0
                            while g == 0:
                                while GPIO.input(machineReadySMEMA) == 0:
                                    print("transporting ...")
                                    time.sleep(0.4)
                                g = g + 1  # breaks loop
                                c.send(transportFinished())  # send transport finished to downstream
                                print("transport finished")
                            print("waiting for stopstransport message from downstream")
                            data7 = c.recv(BUFFER_SIZE1)
                            data8 = getRequest(data7)
                            if data8[2] == "<StopTransport":
                                GPIO.output(led_up_groen, GPIO.HIGH)
                                GPIO.output(led_up_rood, GPIO.LOW)
                                GPIO.output(output, GPIO.HIGH)
                                time.sleep(2)
                                GPIO.output(output, GPIO.LOW)
                                print("transport has stopped ... ")
                                print("ready to repeat process")
except KeyboardInterrupt:
    print("\n" + "session ended by user")
    s1.close()
    print("connection closed")

finally:
    GPIO.cleanup()

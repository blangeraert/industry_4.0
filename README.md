This includes all my code used in my bachelor's thesis and all the made demo's.

Includes code for the driver, Raspberry for the driver and the GUI (main_width_Adjustment_software)

Includes testcode for TCP connections (TCP_connection_test)

Includes Hermes test software without ID tracking (Hermes_code_V2/downstreammachine_testcode_V2 and Hermes_code_V2/upstreammachine_testcode_V2)

Includes Hermes test software WITH ID tracking (Hermes_code_V3_ID_TRACKING/downstreammachine_testdemocode_V3 and Hermes_code_V3_ID_TRACKING/upstreammachine_testdemocode_V3)

Includes code for SMEMA-Hermes-conversion boxes (Hermes_code_V3_ID_TRACKING/downstreammachine_maincode_V2 and Hermes_code_V3_ID_TRACKING/upstreammachine_maincode_V2)

The Hermes_code_V2/downstreammachine_testcode_V2 and Hermes_code_V2/upstreammachine_testcode_V2 was the code used in the demo's WITHOUT ID tracking.

The Hermes_code_V3_ID_TRACKING/downstreammachine_testdemocode_V3 and Hermes_code_V3_ID_TRACKING/upstreammachine_testdemocode_V3 was the code used in the demo's WITH ID tracking!

When a code is used with ID tracking there is another script necessary to connect to a database:
Hermes_code_V3_ID_TRACKING/database_lib.py
Make sure to change the username, password, table, etc. to the desired parameters for your application!



Link to demo's: https://drive.google.com/drive/folders/14MVsazf-HO_-xhuB-o1F1CjoHHe0f8pL?usp=sharing

def giveids(): #weergeeft de id's van de PCB's
   import mysql.connector as mariadb
   import re
   mariadb_connection = mariadb.connect(user='benoit',
                                        password='benoit',
                                        database='pcbs')
   cursor = mariadb_connection.cursor()
   width1 = []
   cursor.execute("SELECT PCB_ID FROM parameters order by id desc limit 1;")
   unwanted = "[),(]"
   for breedte in cursor:
      wanted = re.sub(unwanted, '', str(breedte))
      width1.append(wanted)
   return width1[0]


def insertValue(ID): #wordt gebruikt om waarden in de database te plaatsten
   import mysql.connector as mariadb
   import re
   mariadb_connection = mariadb.connect(user='benoit',
                                        password='benoit',
                                        database='pcbs')
   cursor = mariadb_connection.cursor()
   query = """INSERT INTO parameters (PCB_ID) VALUES (%s);"""
   cursor.execute(query, (ID,))
   mariadb_connection.commit()
   return


def deleteLastEntry(): # verwijderd laatst toegevoegde PCB
   import mysql.connector as mariadb
   import re
   mariadb_connection = mariadb.connect(user='benoit',
                                        password='benoit',
                                        database='pcbs')
   cursor = mariadb_connection.cursor()
   query = """delete from parameters order by id desc limit 1;"""
   cursor.execute(query)
   mariadb_connection.commit()
   return








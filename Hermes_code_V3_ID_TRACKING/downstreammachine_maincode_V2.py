from datetime import datetime
import RPi.GPIO as GPIO
import time
import socket
import select
import database_lib as database


upstream = "192.168.0.153"
TCP_PORT = 50101
BUFFER_SIZE1 = 1024

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

machineReadySMEMA = 19 # wanneer "if machineready == 0:" dan is het signaal laag en is de conveyor zogezegd klaar om een PCB te ontvangen.
boardAvailableSMEMA = 6 # een output, wanneer dit een laag signaal is en dus wanneer de upstream klaar is om een PCB over te brengen kan het transporteren starten.

output = 23 #signaal naar upstream toe om te zeggen dat een PCB aanwezig is om te tranporteren
input = 22 #inputsignaal om info te krijgen wanneer de PCB de conveyor heeft verlaten en zo het proces kan herbeginnen.

led_down_rood = 12
led_down_groen = 20

GPIO.setup(led_down_groen, GPIO.OUT)
GPIO.setup(led_down_rood, GPIO.OUT)

GPIO.setup(machineReadySMEMA, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(boardAvailableSMEMA, GPIO.OUT)

GPIO.setup(output, GPIO.OUT)
GPIO.setup(input, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def getRequest(data):
    request = data.decode("utf-8")
    request1 = request.split(' ')
    return request1


def checkAlive():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    checkAliveResponse = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">"
                          + str.encode('\n') + b" <" + b"CheckAlive Type=" + str.encode("\"") + b"2" + str.encode("\"")
                          + b" /" + b">" + str.encode('\n') + b"</Hermes>") + str.encode('\n')
    return checkAliveResponse


def serviceDescriptionDownstream():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b" <ServiceDescription LaneId=" + str.encode("\"") + b"1" + str.encode("\"") + b" MachineId=" + str.encode("\"") + b"downstream machine" + str.encode("\"") +
               b" Version=" + str.encode("\"") + b"1.2" + str.encode("\"") + b">" + str.encode('\n') +
               b"  <SupportedFeatures />" + str.encode('\n') + b" </ServiceDescription>" + str.encode('\n') + b"</Hermes>") + str.encode('\n')
    return request


def machineReady():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b"<MachineReady FailedBoard=" + str.encode("\"") + b"0" + str.encode("\"") + b" FlippedBoard=" + str.encode("\"") + b"0" + str.encode("\"") + b" />" + str.encode('\n') +
               b"</Hermes>")
    return request


def startTransport():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b"<StartTransport BoardId=" + str.encode("\"") + b"555" + str.encode("\"") + b" />" + str.encode('\n') +
               b"</Hermes>")
    return request


def stopTransport():
    string1 = b"<Hermes Timestamp="
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%M')
    request = (string1 + str.encode("\"") + str.encode(timestamp) + str.encode("\"") + b">" + str.encode('\n') +
               b"<StopTransport TransferState=" + str.encode("\"") + b"3" + str.encode("\"") + b" BoardId=" +
               str.encode("\"") + b"555" + str.encode("\"") + b" />" + str.encode('\n') +
               b"</Hermes>")
    return request


try:
    GPIO.output(led_down_rood, GPIO.LOW)
    GPIO.output(led_down_groen, GPIO.HIGH)
    s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s1.connect((upstream, TCP_PORT))
    print("connected to upstream machine...")
    while True:
        GPIO.output(led_down_rood, GPIO.LOW)
        GPIO.output(led_down_groen, GPIO.HIGH)
        GPIO.output(output, GPIO.LOW)
        data1 = s1.recv(BUFFER_SIZE1)
        response0 = getRequest(data1)
        if response0[2] == "<CheckAlive":
             print("checkAlive message received")
             s1.send(checkAlive())
             print("checkalive message was send to upstream")
             time.sleep(2)
             s1.send(serviceDescriptionDownstream())  # send servicedescription to upstream
             print("servicediscription to upstream was send ...")
             data2 = s1.recv(BUFFER_SIZE1) # wait upstream service description
             response = getRequest(data2)
             if response[4] == "MachineId=\"Upstream":
                print("downstream to upstream connection established ...")
                while True: # while in this loop we have a constantly connected stream, which enables us to handover PCB's with this loop only!
                    if GPIO.input(machineReadySMEMA) == 0:
                        s1.send(machineReady())
                        print("machineReady was send  ...")
                        data3 = s1.recv(BUFFER_SIZE1)  # wait for boardAvailable
                        response2 = getRequest(data3)
                        if response2[2] == "<BoardAvailable":
                            print("Given ID: " + getId(data3))
                            database.insertValue(getId(data3))
                            GPIO.output(led_down_rood, GPIO.HIGH)
                            GPIO.output(led_down_groen, GPIO.LOW)
                            print("board is available from upstream, ready for start transport ...")
                            GPIO.output(boardAvailableSMEMA, GPIO.HIGH) #signaal wordt laag en het transporteren begint
                            print("transport has started")
                            s1.send(startTransport())  # sends a start transport signal
                            time.sleep(1)
                            GPIO.output(boardAvailableSMEMA, GPIO.LOW) #signaal wordt hoog
                            while GPIO.input(machineReadySMEMA) == 0:
                                print("transporting PCB ...")
                                time.sleep(0.5)
                            GPIO.output(led_down_rood, GPIO.LOW)
                            GPIO.output(led_down_groen, GPIO.HIGH)
                            print("transport has stopped") # door detectie van de sensoren op de downstream is het transporteren gestopt
                            data4 = s1.recv(BUFFER_SIZE1)  # wait for transportFinished repsonse
                            respone3 = getRequest(data4)
                            if respone3[2] == "<TransportFinished":
                                s1.send(stopTransport())
                                print("transport finished") # now that the PCB is on the conveyor, its ready to be send to the downstream machine.
                                h = 0
                                while h == 0:
                                    GPIO.output(output, GPIO.HIGH)
                                    print("PCB available on conveyor and waiting until PCB is handed over to the downstream machine")
                                    GPIO.output(led_down_rood, GPIO.LOW)
                                    GPIO.output(led_down_groen, GPIO.HIGH)
                                    time.sleep(0.4)
                                    if GPIO.input(input) == 0:
                                        print("PCB has left conveyor towards downstream") # process can restart!
                                        GPIO.output(output, GPIO.LOW)
                                        h = h + 1
                                    GPIO.output(led_down_rood, GPIO.HIGH)
                                    GPIO.output(led_down_groen, GPIO.LOW)
                                    time.sleep(0.4)
except socket.error as exc:
    print("Caught exception socket.error : %s" % exc)

except KeyboardInterrupt:
    print("\n" + "session ended by user")
    GPIO.output(output, GPIO.LOW)
    s1.close()
    print("connection closed")

finally:
    GPIO.cleanup()
